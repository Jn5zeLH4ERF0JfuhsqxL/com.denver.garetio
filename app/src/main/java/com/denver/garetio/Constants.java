package com.denver.garetio;

public class Constants {


    private Constants() {
    }

    public static final String mainUrl = "https://vondra.website/kmkt6frh";
    public static final String moderationUrl = "https://vondra.website/kmkt6frf";
    public static final String appsFlyerKey = "AcYFTpvc9wT7EErCK548rg";
    public static final String oneSignalAppId = "472cda1d-5463-41d2-a9f1-fdcf73307edb";
    public static final String appName = "com.denver.garetio";
    public static final Boolean debugMode = false;
}
