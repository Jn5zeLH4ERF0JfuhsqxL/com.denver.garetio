/***
 * Name: HomeActivity
 * Description: Activity for the loading screen, also known as the splash screen.
 *
 **/

package com.denver.garetio;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashActivity extends AppCompatActivity {

    // Splash screen timer.
    private static int SPLASH_TIME_OUT = 3000;

    private TextView ivLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Animation for the starting logo.
        ivLogo = (TextView) findViewById(R.id.textView);
        Animation logoAnim = AnimationUtils.loadAnimation(this,R.anim.loading_logo_anim);
        ivLogo.startAnimation(logoAnim);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent homeIntent = new Intent(SplashActivity.this, ConfirmationActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                finish();

            }
        },SPLASH_TIME_OUT);
    }
}